package com.dicv.truck.vo;

import java.math.BigDecimal;

public class VehicleVO {

	private Integer vehicleId;

	private BigDecimal gpsSimNumber;

	private BigDecimal gpsImei;

	private String registrationId;

	private String vin;

	private Integer vehicleTypeId;

	private Integer vehicleCategory;
	
	private String vehicleTypeName;

	private String vehicleCategoryName;

	private String purchaseDate;

	private String description;

	private Integer maxVehicleSpeed;

	private Integer maxPayLoadCapacity;

	private Boolean gpsTranmission = false;

	private Boolean vehicleCanParam = false;

	public Integer getVehicleId() {
		return vehicleId;
	}


	public String getRegistrationId() {
		return registrationId;
	}

	public String getVin() {
		return vin;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public Boolean getGpsTranmission() {
		return gpsTranmission;
	}

	public Boolean getVehicleCanParam() {
		return vehicleCanParam;
	}

	public void setGpsTranmission(Boolean gpsTranmission) {
		this.gpsTranmission = gpsTranmission;
	}

	public void setVehicleCanParam(Boolean vehicleCanParam) {
		this.vehicleCanParam = vehicleCanParam;
	}


	public Integer getVehicleTypeId() {
		return vehicleTypeId;
	}

	public Integer getVehicleCategory() {
		return vehicleCategory;
	}

	public void setVehicleTypeId(Integer vehicleTypeId) {
		this.vehicleTypeId = vehicleTypeId;
	}

	public void setVehicleCategory(Integer vehicleCategory) {
		this.vehicleCategory = vehicleCategory;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public String getDescription() {
		return description;
	}



	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMaxVehicleSpeed() {
		return maxVehicleSpeed;
	}

	public Integer getMaxPayLoadCapacity() {
		return maxPayLoadCapacity;
	}

	public void setMaxVehicleSpeed(Integer maxVehicleSpeed) {
		this.maxVehicleSpeed = maxVehicleSpeed;
	}

	public void setMaxPayLoadCapacity(Integer maxPayLoadCapacity) {
		this.maxPayLoadCapacity = maxPayLoadCapacity;
	}


	public BigDecimal getGpsSimNumber() {
		return gpsSimNumber;
	}


	public BigDecimal getGpsImei() {
		return gpsImei;
	}


	public void setGpsSimNumber(BigDecimal gpsSimNumber) {
		this.gpsSimNumber = gpsSimNumber;
	}


	public void setGpsImei(BigDecimal gpsImei) {
		this.gpsImei = gpsImei;
	}


	public String getVehicleTypeName() {
		return vehicleTypeName;
	}


	public String getVehicleCategoryName() {
		return vehicleCategoryName;
	}


	public void setVehicleTypeName(String vehicleTypeName) {
		this.vehicleTypeName = vehicleTypeName;
	}


	public void setVehicleCategoryName(String vehicleCategoryName) {
		this.vehicleCategoryName = vehicleCategoryName;
	}





}
