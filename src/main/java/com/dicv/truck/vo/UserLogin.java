package com.dicv.truck.vo;

import java.io.Serializable;

public class UserLogin implements Serializable {

	private static final long serialVersionUID = 1L;
	private String userRole;
	private String token;
	private String userName;
	private Integer userId;

	public UserLogin() {

	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
