package com.dicv.truck.util;

/**
 * The class is responsible for maintain record state, either 0/1.
 * 
 * @author seg3kor
 * @version 1.0 12/11/2015
 * 
 */

public enum RecordState {

	IS_NOT_DELETED(0), DELETED(1), IS_ENABLE(1), IS_NOT_ENABLE(0);

	private int state;

	RecordState(int state) {
		this.state = state;

	}

	public int getState() {
		return state;
	}

}
