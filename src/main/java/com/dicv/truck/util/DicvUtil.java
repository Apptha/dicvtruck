package com.dicv.truck.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.dicv.truck.common.exception.DicvTruckException;

public class DicvUtil {

	private static final Logger LOGGER = Logger.getLogger(DicvUtil.class);

	public static String getIpAddress(ServletRequest request, HttpServletRequest httpReq) {
		String ipAddress = httpReq.getHeader("X-FORWARDED-FOR");
		LOGGER.info("USER IP_ADDRESS FROM HEADER " + ipAddress);
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
			LOGGER.info("USER IP_ADDRESS FROM REMOTE" + ipAddress);
		}
		return ipAddress;
	}

	public static Timestamp getTimestamp() {
		java.util.Date date = new java.util.Date();
		return new java.sql.Timestamp(date.getTime());
	}

	public static Date stringToDate(String date) throws DicvTruckException {
		DateFormat dateFormate = new SimpleDateFormat("dd-MMM-yyyy");
		try {
			return (dateFormate.parse(date));
		} catch (ParseException e) {
			LOGGER.log(Level.ERROR, "There was an exception in the server during stringToDate." + e.getMessage());
			throw new DicvTruckException("Could not parse date to the given format!");
		}
	}

	public static Timestamp getCurrentTime() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Timestamp currentDate = Timestamp.valueOf(dateFormat.format(cal.getTime()));
		return currentDate;
	}

	public static Integer doGetPageNo(Integer pagenum) {
		if (pagenum == null) {
			pagenum = 1;
		}
		return pagenum;
	}

	public static Integer doGetPageCount(Integer pagecount) {
		if (pagecount == null) {
			pagecount = 10;
		}
		return pagecount;
	}

	public static Date getPreviousDayTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		return calendar.getTime();
	}

}
