package com.dicv.truck.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dicv.truck.response.Response;
import com.dicv.truck.service.VehicleService;
import com.dicv.truck.vo.CategoryVO;
import com.dicv.truck.vo.TypeVO;
import com.dicv.truck.vo.VehicleVO;

@RestController
@RequestMapping("/truckService/vehicle")
public class VehicleController {

	@Autowired
	private VehicleService vehicleService;

	private static final Logger LOGGER = Logger.getLogger(VehicleController.class);

	// Without Pagination
	@RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
	@ResponseBody
	private Response<List<VehicleVO>> getVehicleList(@PathVariable Integer userId) {

		return vehicleService.getVehicleList(userId);
	}

	@RequestMapping(value = "/user/{userId}/vehicle/{vehicleId}", method = RequestMethod.GET)
	@ResponseBody
	private Response<VehicleVO> getVehicleDetails(@PathVariable Integer userId, @PathVariable Integer vehicleId) {

		return vehicleService.getVehicleDetails(userId, vehicleId);
	}

	@RequestMapping(value = "/create/user/{userId}", method = RequestMethod.POST)
	@ResponseBody
	private Response<Integer> createVehicle(@PathVariable Integer userId, @RequestBody VehicleVO vehicleVO) {

		return vehicleService.createVehicle(userId, vehicleVO);
	}

	@RequestMapping(value = "/type/user/{userId}", method = RequestMethod.GET)
	@ResponseBody
	private Response<List<TypeVO>> getTypeList(@PathVariable Integer userId) {

		return vehicleService.getTypeList(userId);
	}
	
	@RequestMapping(value = "/category/user/{userId}", method = RequestMethod.GET)
	@ResponseBody
	private Response<List<CategoryVO>> getCategoryList(@PathVariable Integer userId) {

		return vehicleService.getCategoryList(userId);
	}

}
