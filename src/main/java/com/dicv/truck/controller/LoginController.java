package com.dicv.truck.controller;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dicv.truck.common.constants.DicvConstants;
import com.dicv.truck.response.Response;
import com.dicv.truck.response.ResponseStatus;
import com.dicv.truck.service.LoginService;
import com.dicv.truck.token.TokenInterceptor;
import com.dicv.truck.vo.UserCredentials;
import com.dicv.truck.vo.UserLogin;

@RestController
@RequestMapping("/truckService")
public class LoginController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private LoginService loginService;

	@Autowired
	private TokenInterceptor tokenInterceptor;

	@RequestMapping(value = "/login", method = { RequestMethod.POST, RequestMethod.OPTIONS })
	@ResponseBody
	public Response<UserLogin> doVerifyCredentials(@RequestBody UserCredentials userCredentails,
			ServletRequest request) {
		Response<UserLogin> response = new Response<UserLogin>();

		try {

			response = loginService.doUserValidation(userCredentails.getUsername(),
					new String(userCredentails.getPassword()), request);
			if (response != null && DicvConstants.SUCCESS.equals(response.getMessage())) {
				response.setStatus(ResponseStatus.SUCCESS);
			} else {
				response.setStatus(ResponseStatus.WARNING);
			}
		} catch (Exception e) {
			LOGGER.info("USER LOGIN METHOD FAILED FOR {} ", userCredentails.getUsername(), e);
			response.setStatus(ResponseStatus.FAILURE);
		}
		if (response.getStatus() == ResponseStatus.SUCCESS) {
			String token = tokenInterceptor.generateToken(response.getData().getUserId());
			LOGGER.debug("Token generated for the user {} and token is {}", userCredentails.getUsername(), token);
			response.getData().setToken(token);
		}
		return response;
	}

	@RequestMapping(value = "/login/user/logout/{userId}", method = RequestMethod.POST)
	public Response<Boolean> userLogout(@PathVariable Integer userId, ServletRequest request) {
		Response<Boolean> response = new Response<Boolean>();
		LOGGER.debug("Logout request for the user {}", userId);
		HttpServletRequest httpReq = (HttpServletRequest) request;
		String authToken = httpReq.getHeader(DicvConstants.AUTH_TOKEN_STRING);
		return response;
	}

	/*
	 * private TokenDetails constructToken(String userName, ServletRequest
	 * request, Integer userId) { TokenDetails details = new TokenDetails();
	 * details.setClientIp(request.getRemoteAddr());
	 * details.setUserName(userName); details.setUserId(userId); return details;
	 * }
	 */

}
