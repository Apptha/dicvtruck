package com.dicv.truck.token;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Payload;
import com.dicv.truck.common.constants.DicvConstants;
import com.dicv.truck.common.exception.DicvTruckException;
import com.dicv.truck.util.DicvUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class TokenInterceptor extends HandlerInterceptorAdapter {

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenInterceptor.class);

	private final ReentrantLock lock = new ReentrantLock();

	public TokenInterceptor() {
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String remoteAddr = httpReq.getRemoteAddr();
		String authToken = httpReq.getHeader(DicvConstants.AUTH_TOKEN_STRING);
		response.addHeader("Access-Control-Expose-Headers", "Auth-Token");
		if (httpReq.getRequestURI().contains("login")) {
			return true;
		}

		Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		Integer userId = null;
		if (pathVariables != null && pathVariables.get("userId") != null) {
			String user = (String) pathVariables.get("userId");
			userId = user != null ? Integer.parseInt(user) : null;
		}

		if (!isValidateToken(authToken, userId)) {
			String msg = "Token Not found !!";
			return warningResponse(httpResponse, remoteAddr, msg);
		}

		return true;
	}

	private boolean warningResponse(HttpServletResponse httpResponse, String remoteAddr, String msg)
			throws IOException {
		String responseStatus = "{\"status\":\"WARNING\",\"data\":[{\"warning\":{\"code\":\"403\",\"message\":\"" + msg
				+ "\"}}]}";
		httpResponse.getWriter().write(responseStatus);
		httpResponse.flushBuffer();
		LOGGER.warn("Token validation failed for remote ip {} ", remoteAddr);
		return false;
	}

	/**
	 * 
	 * @param tokenDetails
	 * @return TokenDetails
	 * @throws DicvTruckException
	 */
	public String generateToken(Integer userId) {
		String token = null;
		try {
			Algorithm algorithm = Algorithm.HMAC256("Dicv0=MCZaqj6J5/a2DIMKdPwig==");
			long nowMillis = System.currentTimeMillis();
			Date issueAt = new Date(nowMillis);
			Date eexpiresAt = new Date(nowMillis + 60 * 60 * 1000);
			token = JWT.create().withSubject(userId.toString()).withIssuedAt(issueAt).withExpiresAt(eexpiresAt)
					.withIssuer("auth0").sign(algorithm);

		} catch (UnsupportedEncodingException exception) {
			LOGGER.warn("Failed in generateToken...");
		} catch (JWTCreationException exception) {
			LOGGER.warn("Failed in generateToken...");
		} catch (Exception ex) {
			LOGGER.error("Exception in JWT");
		}
		/*
		 * JWTSigner signer = new JWTSigner("Dicv_Truck_Service_Daimler");
		 * Map<String, Object> claims = new HashMap<String, Object>();
		 * claims.put("user", tokenDetails.getUserName()); JWTSigner.Options
		 * option = new JWTSigner.Options();
		 * option.setAlgorithm(Algorithm.HS256); //
		 * option.setExpirySeconds(15000); option.setIssuedAt(true);
		 * option.setJwtId(true); option.setNotValidBeforeLeeway(3); // Setting
		 * the values to the object tokenDetails.setToken(signer.sign(claims,
		 * option)); Calendar now = Calendar.getInstance(); // add minutes to
		 * current date using Calendar.add method now.add(Calendar.MINUTE, 30);
		 * tokenDetails.setTokenExpiryTime(now.getTimeInMillis());
		 * TokenCache.doAddTokenDetails(tokenDetails);
		 */
		return token;
	}

	private boolean isValidateToken(String token, Integer userId) {
		if (token == null || token.isEmpty()) {
			LOGGER.info("Faled in validating Token,Either token is null or empty...");
			return false;
		}
		try {
			long nowMillis = System.currentTimeMillis();
			Date currentDate = new Date(nowMillis);
			DecodedJWT jwt = JWT.decode(token);
			if (jwt != null && jwt.getSubject() != null && jwt.getAlgorithm().equals("HS256")
					&& (currentDate.before(jwt.getExpiresAt()))) {
				if (Integer.parseInt(jwt.getSubject().toString()) == userId) {
					return true;
				}
			}
			LOGGER.info("Token Expired  Date " + jwt.getExpiresAt());
			LOGGER.info("Returning default value for isValidateToken method...");
		} catch (IllegalArgumentException e) {
			LOGGER.error("Returning default value for isValidateToken method...");
			e.printStackTrace();
		} catch (JWTDecodeException exception) {
			LOGGER.error("Returning default value for isValidateToken method...");
		} catch (Exception ex) {
			LOGGER.error("Exception in JWT");
		}
		return false;
	}

}
