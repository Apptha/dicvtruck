/*package com.dicv.truck.token;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TokenCache {

	private static Map<String, TokenDetails> tokenMap = new HashMap<String, TokenDetails>();

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenCache.class);

	private final static ReentrantLock lock = new ReentrantLock();

	public static void doLoadLatestTokens() {

	}

	public static TokenDetails findTokenDetailsByID(String authToken) {
		lock.lock();
		try {
			if (tokenMap != null) {
				TokenDetails token = tokenMap.get(authToken);
				return token;
			}
		} catch (Exception e) {
			LOGGER.info("Exception in Getting Token Details");
		} finally {
			lock.unlock();
		}
		return null;

	}

	public static void doAddTokenDetails(TokenDetails token) {
		lock.lock();
		try {
			if (tokenMap != null) {
				long currentTime = System.currentTimeMillis();
				tokenMap.put(token.getToken(), token);
			}
		} catch (Exception e) {
			LOGGER.info("Exception in Adding Token Details");
		} finally {
			lock.unlock();
		}

	}

	public static void doUpdateTokenDetails(TokenDetails token) {
		lock.lock();
		try {
			if (tokenMap != null) {
				long currentTime = System.currentTimeMillis();
				tokenMap.put(token.getToken(), token);
			}
		} catch (Exception e) {
			LOGGER.info("Exception in updating Token Details");
		} finally {
			lock.unlock();
		}

	}

	public static void doDeleteTokenDetails(TokenDetails token) {
		lock.lock();
		try {
			if (tokenMap != null) {
				tokenMap.remove(token.getToken());
			}
		} catch (Exception e) {
			LOGGER.info("Exception in Deleting Token Details");
		} finally {
			lock.unlock();
		}

	}

	public static Map<String, TokenDetails> doGetTokenMap() {
		lock.lock();
		try {
			if (tokenMap != null) {
				return tokenMap;
			}
		} catch (Exception e) {
			LOGGER.info("Exception in Getting Token Map for Scheduling");
		} finally {
			lock.unlock();
		}
		return null;

	}
}
*/