package com.dicv.truck.dao.gpsImei;

import java.math.BigDecimal;
import java.util.List;

import com.dicv.truck.common.exception.DicvTruckException;
import com.dicv.truck.dao.pojo.GpsImei;

public interface GpsDAO {

	public GpsImei getGpsImeiDetails(BigDecimal imeiNo) throws DicvTruckException;

	public GpsImei getGpsImeiDetails(Integer vehicleId) throws DicvTruckException;

	public Integer crudGpsImei(GpsImei gpsImei) throws DicvTruckException;

	public boolean checkVehicleCanParam(Integer vehicleId) throws DicvTruckException;

	public boolean checkGpsTranmision(BigDecimal imeiNo) throws DicvTruckException;

	public boolean checkImeiExist(BigDecimal imeiNo, Integer vehicleId) throws DicvTruckException;

	public List<Integer> getVehicleCanParam() throws DicvTruckException;

	public List<BigDecimal> checkGpsTranmision() throws DicvTruckException;

}
