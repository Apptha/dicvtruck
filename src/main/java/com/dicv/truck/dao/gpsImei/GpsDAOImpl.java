package com.dicv.truck.dao.gpsImei;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dicv.truck.common.constants.DicvConstants;
import com.dicv.truck.common.exception.DicvTruckException;
import com.dicv.truck.dao.pojo.GpsImei;

@Repository
public class GpsDAOImpl implements GpsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private static final Logger LOGGER = Logger.getLogger(GpsDAOImpl.class);

	@Override
	public Integer crudGpsImei(GpsImei gpsImei) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		Integer gpsId = null;
		try {
			session.beginTransaction();
			if (gpsImei.getGpsImeiId() == null) {
				gpsId = (Integer) session.save(gpsImei);
			} else {
				session.merge(gpsImei);
			}
			session.getTransaction().commit();
		} catch (PersistenceException ex) {
			throw new DicvTruckException(DicvConstants.GPS_IMEI_CRUD, ex);
		} catch (Exception ex) {
			throw new DicvTruckException(DicvConstants.GPS_IMEI_CRUD, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return gpsId;
	}

	@Override
	public GpsImei getGpsImeiDetails(BigDecimal imeiNo) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("select c from GpsImei c where c.gpsImei=:imeiNo and c.isDeleted=0");
			query.setParameter("imeiNo", imeiNo);
			List<GpsImei> list = query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}

		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	@Override
	public GpsImei getGpsImeiDetails(Integer vehicleId) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session
					.createQuery("select c from GpsImei c where c.vehicle.vehicleId=:vehicleId and c.isDeleted=0");
			query.setParameter("vehicleId", vehicleId);
			List<GpsImei> list = query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}

		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	@Override
	public boolean checkVehicleCanParam(Integer vehicleId) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session
					.createQuery("select count(v) from VehicleCanParam v where v.vehicle.vehicleId=:vehicleId");
			query.setParameter("vehicleId", vehicleId);
			Long count = (Long) query.uniqueResult();
			if (count != null && count > 0) {
				return true;
			}

		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return false;
	}

	@Override
	public boolean checkGpsTranmision(BigDecimal imeiNo) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("select count(c) from GpsVehicleParameter c where c.gpsImei=:imeiNo");
			query.setParameter("imeiNo", imeiNo);
			Long count = (Long) query.uniqueResult();
			if (count != null && count > 0) {
				return true;
			}

		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return false;
	}

	@Override
	public boolean checkImeiExist(BigDecimal imeiNo, Integer vehicleId) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			String queryStr = "select count(c) from GpsImei c where c.gpsImei=:imeiNo and c.isDeleted=0";
			if (vehicleId != null)
				queryStr = queryStr + " and c.vehicle.vehicleId!=:vehicleId";
			Query query = session.createQuery(queryStr);
			query.setParameter("imeiNo", imeiNo);
			if (vehicleId != null)
				query.setParameter("vehicleId", vehicleId);
			Long count = (Long) query.uniqueResult();
			if (count != null && count > 0) {
				return true;
			}

		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.GPS_IMEI, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return false;
	}

	@Override
	public List<BigDecimal> checkGpsTranmision() throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("select DISTINCT(gpsImei) from GpsVehicleParameter c ");
			List<BigDecimal> list = query.list();
			if (list != null && list.size() > 0) {
				LOGGER.info("Total GPS IMEI Count " + list.size());
				return list;
			}

		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return new ArrayList<BigDecimal>(Collections.EMPTY_LIST);

	}

	@Override
	public List<Integer> getVehicleCanParam() throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("select v.vehicle.vehicleId from VehicleCanParam v ");
			List<Integer> list = (List<Integer>) query.list();
			if (list != null && list.size() > 0) {
				return list;
			}

		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return new ArrayList<Integer>(Collections.EMPTY_LIST);

	}

}
