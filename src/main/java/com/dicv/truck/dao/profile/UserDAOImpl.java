package com.dicv.truck.dao.profile;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dicv.truck.common.constants.DicvConstants;
import com.dicv.truck.common.exception.DicvTruckException;
import com.dicv.truck.dao.pojo.DicvUser;
import com.dicv.truck.dao.pojo.UserProfile;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(UserDAOImpl.class);

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public UserProfile doGetUserProfileDetails(String userName) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("select c from UserProfile c where c.userName=:userName");
			query.setParameter("userName", userName);
			List<UserProfile> list = (List<UserProfile>) query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.ERR_USER_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	@Override
	public DicvUser getRootAdminUser() throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("select c from DicvUser c where c.userId=:userId");
			query.setParameter("userId", 2);
			List<DicvUser> list = (List<DicvUser>) query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.ERR_USER_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	@Override
	public UserProfile doGetUserProfileDetails(Integer userId) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("select c from UserProfile c where c.userId=:userId");
			query.setParameter("userId", userId);
			List<UserProfile> list = (List<UserProfile>) query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.ERR_USER_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

}
