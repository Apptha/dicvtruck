package com.dicv.truck.dao.profile;

import com.dicv.truck.common.exception.DicvTruckException;
import com.dicv.truck.dao.pojo.DicvUser;
import com.dicv.truck.dao.pojo.UserProfile;

public interface UserDAO {

	public UserProfile doGetUserProfileDetails(String userName) throws DicvTruckException;
	
	public UserProfile doGetUserProfileDetails(Integer userId) throws DicvTruckException;
	
	public DicvUser getRootAdminUser() throws DicvTruckException;

}
