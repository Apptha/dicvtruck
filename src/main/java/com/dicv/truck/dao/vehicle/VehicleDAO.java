package com.dicv.truck.dao.vehicle;

import java.util.List;

import com.dicv.truck.common.exception.DicvTruckException;
import com.dicv.truck.dao.pojo.DicvCategory;
import com.dicv.truck.dao.pojo.DicvType;
import com.dicv.truck.dao.pojo.Vehicle;

public interface VehicleDAO {

	public Vehicle getVehicleDetails(Integer vehicleId) throws DicvTruckException;

	public Integer crudVehicle(Vehicle vehicle) throws DicvTruckException;

	public List<Vehicle> getVehicleList(Integer userId) throws DicvTruckException;

	public List<DicvType> getTypeList(Integer userId) throws DicvTruckException;

	public Boolean checkRegistrationAvailable(String registrationId,Integer vehicleId) throws DicvTruckException;

	public Boolean checkVINAvailable(String vin,Integer vehicleId) throws DicvTruckException;

	public List<DicvCategory> getCategoryList(Integer userId) throws DicvTruckException;

	public DicvType getType(Integer typeId,Integer userId) throws DicvTruckException;

	public DicvCategory getCategory(Integer categoryId,Integer userId) throws DicvTruckException;


}
