package com.dicv.truck.dao.vehicle;

import java.util.List;

import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dicv.truck.common.constants.DicvConstants;
import com.dicv.truck.common.exception.DicvTruckException;
import com.dicv.truck.dao.pojo.DicvCategory;
import com.dicv.truck.dao.pojo.DicvType;
import com.dicv.truck.dao.pojo.Vehicle;

@Repository
public class VehicleDAOImpl implements VehicleDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private static final Logger LOGGER = Logger.getLogger(VehicleDAOImpl.class);

	@Override
	public Vehicle getVehicleDetails(Integer vehicleId) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			Query query = session.createQuery("select c from Vehicle c where c.vehicleId=:vehicleId and c.isDeleted=0");
			query.setParameter("vehicleId", vehicleId);
			List<Vehicle> list = query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}

		} catch (Exception e) {
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	@Override
	public Integer crudVehicle(Vehicle vehicle) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		Integer gpsId = null;
		try {
			session.beginTransaction();
			if (vehicle.getVehicleId() == null) {
				gpsId = (Integer) session.save(vehicle);
			} else {
				session.merge(vehicle);
			}
			session.getTransaction().commit();
		} catch (PersistenceException ex) {
			throw new DicvTruckException(DicvConstants.VEH_CRUD_DETAILS, ex);
		} catch (Exception ex) {
			throw new DicvTruckException(DicvConstants.VEH_CRUD_DETAILS, ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return gpsId;
	}

	@Override
	public List<Vehicle> getVehicleList(Integer userId) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			String queryStr = "select c from Vehicle c where  c.isDeleted=0  order by c.modifiedDateTime desc";
			Query query = session.createQuery(queryStr);
			List<Vehicle> list = query.list();
			if (list != null && list.size() > 0) {
				return list;
			}
		} catch (Exception e) {
			LOGGER.info("Exception " + e);
			e.printStackTrace();
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	@Override
	public Boolean checkRegistrationAvailable(String registrationId, Integer vehicleId) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {

			String queryStr = "Select count(v) FROM Vehicle v where v.registrationId=:registrationId  and v.isDeleted=0";
			if (vehicleId != null) {
				queryStr = queryStr + " and v.vehicleId!=:vehicleId";
			}

			Query query = session.createQuery(queryStr);
			query.setParameter("registrationId", registrationId);
			if (vehicleId != null) {
				query.setParameter("vehicleId", vehicleId);
			}
			Long count = (Long) query.uniqueResult();
			if (count != null && count > 0) {
				return false;
			}
		} catch (Exception e) {
			LOGGER.info("Exception in Getting RegistrationId Condition " + e);
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return true;

	}

	@Override
	public Boolean checkVINAvailable(String vin, Integer vehicleId) throws DicvTruckException {
		Session session = sessionFactory.openSession();
		try {
			String queryStr = "Select count(v) FROM Vehicle v where  v.vin=:vin and v.isDeleted=0";
			if (vehicleId != null) {
				queryStr = queryStr + " and v.vehicleId!=:vehicleId";
			}
			Query query = session.createQuery(queryStr);
			query.setParameter("vin", vin);
			if (vehicleId != null) {
				query.setParameter("vehicleId", vehicleId);
			}
			Long count = (Long) query.uniqueResult();
			if (count != null && count > 0) {
				return false;
			}
		} catch (Exception e) {
			LOGGER.info("Exception in Getting Vin  Condition " + e);
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return true;

	}

	@Override
	public List<DicvType> getTypeList(Integer userId) throws DicvTruckException {

		Session session = sessionFactory.openSession();
		try {
			String queryStr = "select t from DicvType t  where t.subTypeId=0 and t.isDeleted=0 and t.createdByUser.userId =:userId";
			Query query = session.createQuery(queryStr);
			query.setParameter("userId", userId);
			List<DicvType> list = query.list();
			if (list != null && list.size() > 0) {
				return list;
			}
		} catch (Exception e) {
			LOGGER.info("Exception " + e);
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	@Override
	public List<DicvCategory> getCategoryList(Integer userId) throws DicvTruckException {

		Session session = sessionFactory.openSession();
		try {
			String queryStr = "select t from DicvCategory t  where t.isDeleted=0 and t.createdByUser=:userId";
			Query query = session.createQuery(queryStr);
			query.setParameter("userId", userId);
			List<DicvCategory> list = query.list();
			if (list != null && list.size() > 0) {
				return list;
			}
		} catch (Exception e) {
			LOGGER.info("Exception " + e);
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	@Override
	public DicvType getType(Integer typeId, Integer userId) throws DicvTruckException {

		Session session = sessionFactory.openSession();
		try {
			String queryStr = "select t from DicvType t  where t.typeId=:typeId and t.isDeleted=0";
			Query query = session.createQuery(queryStr);
			query.setParameter("typeId", typeId);
			List<DicvType> list = query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			LOGGER.info("Exception " + e);
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	@Override
	public DicvCategory getCategory(Integer categoryId, Integer userId) throws DicvTruckException {

		Session session = sessionFactory.openSession();
		try {
			String queryStr = "select t from DicvCategory t  where  t.categoryId=:categoryId and  t.isDeleted=0";
			Query query = session.createQuery(queryStr);
			query.setParameter("categoryId", categoryId);
			List<DicvCategory> list = query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			LOGGER.info("Exception " + e);
			throw new DicvTruckException(DicvConstants.VEH_DETAILS, e);

		} finally {

			if (session != null) {
				session.close();
			}
		}
		return null;
	}

}
