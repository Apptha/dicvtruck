package com.dicv.truck.dao.pojo;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Where;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The persistent class for the GPS_IMEI database table.
 * 
 */
@Entity
@Table(name = "GPS_IMEI")
public class GpsImei implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "GPS_IMEI_GPSIMEIID_GENERATOR", sequenceName = "GPS_IMEI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GPS_IMEI_GPSIMEIID_GENERATOR")
	@Column(name = "GPS_IMEI_ID")
	private Integer gpsImeiId;

	@Column(name = "GPS_IMEI")
	private BigDecimal gpsImei;

	@Column(name = "GPS_PROVIDER")
	private String gpsProvider;

	@Column(name = "GPS_SIM_NUMBER")
	private BigDecimal gpsSimNumber;

	@Column(name = "TABLET_IMEI")
	private BigDecimal tabletImei;

	@Column(name = "TABLET_PROVIDER")
	private String tabletProvider;

	@Column(name = "TABLET_SIM_NUMBER")
	private BigDecimal tabletSimNumber;

	@Column(name = "CREATED_DATE")
	private Timestamp createdDate;

	@Column(name = "MODIFIED_DATE")
	private Timestamp updatedDate;

	@Column(name = "IS_DELETED")
	private int isDeleted;

	// bi-directional many-to-one association to Vehicle
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "VEHICLE_ID")
	private Vehicle vehicle;

	@Column(name = "CREATED_BY")
	private Integer dicvUserCreatedBy;

	@Column(name = "UPDATED_BY")
	private Integer dicvUserUpdatedBy;

	public GpsImei() {
	}

	public Integer getGpsImeiId() {
		return this.gpsImeiId;
	}

	public void setGpsImeiId(Integer gpsImeiId) {
		this.gpsImeiId = gpsImeiId;
	}

	public BigDecimal getGpsImei() {
		return gpsImei;
	}

	public String getGpsProvider() {
		return gpsProvider;
	}

	public BigDecimal getGpsSimNumber() {
		return gpsSimNumber;
	}

	public BigDecimal getTabletImei() {
		return tabletImei;
	}

	public String getTabletProvider() {
		return tabletProvider;
	}

	public BigDecimal getTabletSimNumber() {
		return tabletSimNumber;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public Integer getDicvUserCreatedBy() {
		return dicvUserCreatedBy;
	}

	public Integer getDicvUserUpdatedBy() {
		return dicvUserUpdatedBy;
	}

	public void setGpsImei(BigDecimal gpsImei) {
		this.gpsImei = gpsImei;
	}

	public void setGpsProvider(String gpsProvider) {
		this.gpsProvider = gpsProvider;
	}

	public void setGpsSimNumber(BigDecimal gpsSimNumber) {
		this.gpsSimNumber = gpsSimNumber;
	}

	public void setTabletImei(BigDecimal tabletImei) {
		this.tabletImei = tabletImei;
	}

	public void setTabletProvider(String tabletProvider) {
		this.tabletProvider = tabletProvider;
	}

	public void setTabletSimNumber(BigDecimal tabletSimNumber) {
		this.tabletSimNumber = tabletSimNumber;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public void setDicvUserCreatedBy(Integer dicvUserCreatedBy) {
		this.dicvUserCreatedBy = dicvUserCreatedBy;
	}

	public void setDicvUserUpdatedBy(Integer dicvUserUpdatedBy) {
		this.dicvUserUpdatedBy = dicvUserUpdatedBy;
	}

	


}