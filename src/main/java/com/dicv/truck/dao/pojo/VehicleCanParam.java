package com.dicv.truck.dao.pojo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name = "VEHICLE_CAN_PARAM")
public class VehicleCanParam implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9038255906553593221L;

	@Id
	@SequenceGenerator(name = "VEH_CAN_PARAM_ID_GENERATOR", sequenceName = "VEH_CAN_PARAM_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VEH_CAN_PARAM_ID_GENERATOR")
	@Column(name = "VEH_CAN_PARAM_ID")
	private Integer vehicleCanParamId;

	@Column(name = "VEHICLE_ENGINE_RPM")
	private Double vehicleEngineRpm;

	@Column(name = "VEHICLE_SPEED_CAN")
	private Double vehicleSpeedCan;

	@Column(name = "FUEL_TANK_ABSOLUTE_VALUE")
	private Double fuelTankAbsoluteValue;

	@Column(name = "UPDATED_DATE_TIME")
	private Calendar updatedDateTime;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "VEHICLE_ID")
	private Vehicle vehicle;

	public Integer getVehicleCanParamId() {
		return vehicleCanParamId;
	}

	public Double getVehicleEngineRpm() {
		return vehicleEngineRpm;
	}

	public Double getVehicleSpeedCan() {
		return vehicleSpeedCan;
	}

	public Double getFuelTankAbsoluteValue() {
		return fuelTankAbsoluteValue;
	}

	public Calendar getUpdatedDateTime() {
		return updatedDateTime;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicleCanParamId(Integer vehicleCanParamId) {
		this.vehicleCanParamId = vehicleCanParamId;
	}

	public void setVehicleEngineRpm(Double vehicleEngineRpm) {
		this.vehicleEngineRpm = vehicleEngineRpm;
	}

	public void setVehicleSpeedCan(Double vehicleSpeedCan) {
		this.vehicleSpeedCan = vehicleSpeedCan;
	}

	public void setFuelTankAbsoluteValue(Double fuelTankAbsoluteValue) {
		this.fuelTankAbsoluteValue = fuelTankAbsoluteValue;
	}

	public void setUpdatedDateTime(Calendar updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
}