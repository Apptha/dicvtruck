package com.dicv.truck.dao.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the VEHICLE database table.
 */
@Entity
@Table(name = "VEHICLE")
public class Vehicle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "VEHICLE_VEHICLEID_GENERATOR", sequenceName = "VEHICLE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VEHICLE_VEHICLEID_GENERATOR")
	@Column(name = "VEHICLE_ID")
	private Integer vehicleId;

	@Column(name = "REGISTRATION_ID")
	private String registrationId;

	@Column(name = "RUNNING_STATUS")
	private String runningStatus;

	@Column(name = "VEHICLE_STATUS")
	private String vehicleStatus;

	@Column(name = "VIN")
	private String vin;

	@Column(name = "ENABLED")
	private Integer enabled;

	@Column(name = "IS_DELETED")
	private Integer isDeleted;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Column(name = "USER_ID")
	private Integer userId;

	@Column(name = "PURCHASE_DATE")
	private Timestamp purchaseDate;

	@Column(name = "MAX_PAYLOAD_CAPACITY")
	private Integer maxPayloadCapacity;

	@Column(name = "MODIFIED_DATE_TIME")
	private Timestamp modifiedDateTime;

	@Column(name = "VEHICLE_UPDATE_TIME")
	private Timestamp vehicleUpdateTime;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "VEH_AVG_SPEED")
	private Integer vehicleMaxSpeed;

	// bi-directional many-to-one association to GpsImei
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "vehicle")
	private GpsImei gpsImei;

	// bi-directional many-to-one association to DicvUser
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "PROFILE_ID")
	private UserProfile userProfile;

	// bi-directional many-to-one association to VehicleCanParam
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "vehicle")
	private VehicleCanParam vehicleCanParam;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "CATEGORY_ID")
	private DicvCategory dicvCategory;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "VEHICLE_TYPE_ID")
	private DicvType dicvType;

	public Integer getVehicleId() {
		return vehicleId;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public String getRunningStatus() {
		return runningStatus;
	}

	public String getVehicleStatus() {
		return vehicleStatus;
	}

	public String getVin() {
		return vin;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public Timestamp getModifiedDateTime() {
		return modifiedDateTime;
	}

	public Timestamp getVehicleUpdateTime() {
		return vehicleUpdateTime;
	}

	public GpsImei getGpsImei() {
		return gpsImei;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public VehicleCanParam getVehicleCanParam() {
		return vehicleCanParam;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public void setRunningStatus(String runningStatus) {
		this.runningStatus = runningStatus;
	}

	public void setVehicleStatus(String vehicleStatus) {
		this.vehicleStatus = vehicleStatus;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public void setModifiedDateTime(Timestamp modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}

	public void setVehicleUpdateTime(Timestamp vehicleUpdateTime) {
		this.vehicleUpdateTime = vehicleUpdateTime;
	}

	public void setGpsImei(GpsImei gpsImei) {
		this.gpsImei = gpsImei;
	}

	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

	public void setVehicleCanParam(VehicleCanParam vehicleCanParam) {
		this.vehicleCanParam = vehicleCanParam;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Timestamp getPurchaseDate() {
		return purchaseDate;
	}

	public String getDescription() {
		return description;
	}

	public void setPurchaseDate(Timestamp purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMaxPayloadCapacity() {
		return maxPayloadCapacity;
	}

	public Integer getVehicleMaxSpeed() {
		return vehicleMaxSpeed;
	}

	public void setMaxPayloadCapacity(Integer maxPayloadCapacity) {
		this.maxPayloadCapacity = maxPayloadCapacity;
	}

	public void setVehicleMaxSpeed(Integer vehicleMaxSpeed) {
		this.vehicleMaxSpeed = vehicleMaxSpeed;
	}

	public DicvCategory getDicvCategory() {
		return dicvCategory;
	}

	public DicvType getDicvType() {
		return dicvType;
	}

	public void setDicvCategory(DicvCategory dicvCategory) {
		this.dicvCategory = dicvCategory;
	}

	public void setDicvType(DicvType dicvType) {
		this.dicvType = dicvType;
	}

}