package com.dicv.truck.dao.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Where;


/**
 * The persistent class for the DICV_USER database table.
 */
@Entity
@Table(name = "DICV_USER")
public class DicvUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "DICV_USER_SEQ", sequenceName = "DICV_USER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DICV_USER_SEQ")
	@Column(name = "USER_ID")
	private Integer userId;

	@Column(name = "CREATED_BY")
	private Integer createdBy;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "RECORD_STATUS")
	private String recordStatus;

	@Column(name = "UPDATED_BY")
	private Integer updatedBy;

	@Column(name = "USER_NAME")
	private String userName;

	public Integer getUserId() {
		return userId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public String getPassword() {
		return password;
	}

	public String getRecordStatus() {
		return recordStatus;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}