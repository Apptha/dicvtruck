package com.dicv.truck.dao.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the GPS_VEHICLE_PARAMETERS database table.
 * 
 */
@Entity
@Table(name = "GPS_VEHICLE_PARAMETERS")
public class GpsVehicleParameter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 859668400211485742L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GPS_VEH_PARAM_SEQ")
	@SequenceGenerator(name = "GPS_VEH_PARAM_SEQ", sequenceName = "GPS_VEH_PARAM_SEQ")
	@Column(name = "GPS_VEH_PARAM_ID")
	private int gpsVehicleParamId;

	@Column(name = "CAN_COOLANT_TEMP")
	private Float canCoolantTemp;

	@Column(name = "CREATED_ON")
	private Timestamp createdOn;

	@Column(name = "GPS_ALTITUDE")
	private Double gpsAltitude;

	@Column(name = "GPS_DATE")
	private Date gpsDate;

	@Column(name = "GPS_DATE_ISO")
	private Date gpsDateISO;

	@Column(name = "GPS_IMEI")
	private BigDecimal gpsImei;

	@Column(name = "GPS_LATITUDE")
	private Double gpsLatitude;

	@Column(name = "GPS_LONGITUDE")
	private Double gpsLongitude;

	@Column(name = "GPS_PROCESS_STATUS")
	private Integer gpsProcessStatus;

	@Column(name = "GPS_TIME")
	private Timestamp gpsTime;

	@Column(name = "GPS_TIME_ISO")
	private Timestamp gpsTimeISO;

	@Column(name = "GPS_UPTIME")
	private Integer gpsUptime;

	@Column(name = "LAST_UPDATED_ON")
	private Timestamp lastUpdatedOn;

	@Column(name = "VEHICLE_VARIANT")
	private String vehiclevariant;

	@Column(name = "CAN_VEHICLE_UPDATED_ON")
	private Timestamp vehicleLastUpdateON;

	@Column(name = "ENGINE_ON")
	private Integer engineON;

	public int getGpsVehicleParamId() {
		return gpsVehicleParamId;
	}

	public Float getCanCoolantTemp() {
		return canCoolantTemp;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public Double getGpsAltitude() {
		return gpsAltitude;
	}

	public Date getGpsDate() {
		return gpsDate;
	}

	public Date getGpsDateISO() {
		return gpsDateISO;
	}


	public Double getGpsLatitude() {
		return gpsLatitude;
	}

	public Double getGpsLongitude() {
		return gpsLongitude;
	}

	public Integer getGpsProcessStatus() {
		return gpsProcessStatus;
	}

	public Timestamp getGpsTime() {
		return gpsTime;
	}

	public Timestamp getGpsTimeISO() {
		return gpsTimeISO;
	}

	public Integer getGpsUptime() {
		return gpsUptime;
	}

	public Timestamp getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public String getVehiclevariant() {
		return vehiclevariant;
	}

	public Timestamp getVehicleLastUpdateON() {
		return vehicleLastUpdateON;
	}

	public Integer getEngineON() {
		return engineON;
	}

	public void setGpsVehicleParamId(int gpsVehicleParamId) {
		this.gpsVehicleParamId = gpsVehicleParamId;
	}

	public void setCanCoolantTemp(Float canCoolantTemp) {
		this.canCoolantTemp = canCoolantTemp;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public void setGpsAltitude(Double gpsAltitude) {
		this.gpsAltitude = gpsAltitude;
	}

	public void setGpsDate(Date gpsDate) {
		this.gpsDate = gpsDate;
	}

	public void setGpsDateISO(Date gpsDateISO) {
		this.gpsDateISO = gpsDateISO;
	}


	public void setGpsLatitude(Double gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}

	public void setGpsLongitude(Double gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}

	public void setGpsProcessStatus(Integer gpsProcessStatus) {
		this.gpsProcessStatus = gpsProcessStatus;
	}

	public void setGpsTime(Timestamp gpsTime) {
		this.gpsTime = gpsTime;
	}

	public void setGpsTimeISO(Timestamp gpsTimeISO) {
		this.gpsTimeISO = gpsTimeISO;
	}

	public void setGpsUptime(Integer gpsUptime) {
		this.gpsUptime = gpsUptime;
	}

	public void setLastUpdatedOn(Timestamp lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public void setVehiclevariant(String vehiclevariant) {
		this.vehiclevariant = vehiclevariant;
	}

	public void setVehicleLastUpdateON(Timestamp vehicleLastUpdateON) {
		this.vehicleLastUpdateON = vehicleLastUpdateON;
	}

	public void setEngineON(Integer engineON) {
		this.engineON = engineON;
	}

	public BigDecimal getGpsImei() {
		return gpsImei;
	}

	public void setGpsImei(BigDecimal gpsImei) {
		this.gpsImei = gpsImei;
	}
}