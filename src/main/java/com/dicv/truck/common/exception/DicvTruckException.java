package com.dicv.truck.common.exception;

public class DicvTruckException extends Exception {

	private String errorCode = null;

	private String errorDesc = null;

	private Throwable throwable = null;

	public DicvTruckException(String errorCode) {
		super(errorCode);
		this.errorCode = errorCode;

	}

	public DicvTruckException(Throwable throwable) {
		super(throwable);
		this.throwable = throwable;

	}

	public DicvTruckException(String errorCode, String errorMsg) {
		super(errorMsg);
		this.errorCode = errorCode;
		this.errorDesc = errorMsg;
	}

	public DicvTruckException(String errorCode, Throwable throwable) {
		super(errorCode, throwable);
		this.errorCode = errorCode;
		this.throwable = throwable;

	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}
}
