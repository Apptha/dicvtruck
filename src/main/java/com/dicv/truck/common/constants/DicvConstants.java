package com.dicv.truck.common.constants;

public final class DicvConstants {

	public static final String USER_NAME_INVALID = "userNameInvalid";
	public static final String PASSWORD_INVALID = "PASSWORD_INVALID";
	public static final String LOGIN_SUCCESS = "SUCCESS";
	public static final String USER_UNAVAILABLE = "USERNAME_FALLED";
	public static final String BLOCKED = "USER_BLOCKED";
	public static final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	public static final String IS_CACHEABLE_FLAG = "isCacheable";
	public static final String PASSWORD_EXPIRES_IN = "passwordExpiryDays";
	public static final String AUTH_TOKEN_STRING = "Auth-Token";
	public static final String USER_NAME = "User-Name";
	public static final String SUCCESS = "SUCCESS";
	public static final String ROOT_ADMIN = "Admin";
	public static final String AUTHENTICATION_ERROR = "Exception in Getting Details";
	public static final String LOGIN_ERROR = "Exception in Getting User LoginService Details";
	public static final String ERR_USER_DETAILS = "Exception in Getting User  Details";
	public static final String VEH_DETAILS = "Exception in Getting Vehicle  Details";
	public static final String VEH_CRUD_DETAILS = "Exception in  Vehicle CRUD";
	public static final String NO_RECORDS_FOUND = "No Records Found";
	public static final String IMEI_ALREADY_EXIST = "Gps Imei No Already Exist";
	public static final String MANDATORY_FIELDS_MISSING = "Please provide all Mandatory Fields";
	public static final String GPS_IMEI_CRUD = "Exception in Adding Gps Imei Details";
	public static final String GPS_IMEI = "Exception in Getting Gps Imei Details";
	public static final String VEHICLE_ADD = "Vehicle Added Successfully";
	public static final String VEHICLE_UPDATE = "Vehicle Updated Successfully";
	public static final String USER_NO_PREVILAGE = "User Not have Previlage";
	public static final String VEH_CANNOT_EDIT = "Vehicle Cannot be edited";
	public static final String IMEI_LENGTH = "The IMEI number should have length of 15";
	public static final String VIN_LENGTH = "The VIN should have length of 17";
	public static final String VIN_NOT_AVAILABLE = "The VIN  is already associated to different vehicle.";
	public static final String REG_NOT_AVAILABLE = "The registration id is already associated to different vehicle.";
	public static final String CATEGORY_VALID = "Please input valid category";
	public static final String VALID_VEH_TYPE = "Please provide valid vehicle type";

}
