package com.dicv.truck.service;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dicv.truck.common.constants.DicvConstants;
import com.dicv.truck.dao.pojo.UserProfile;
import com.dicv.truck.dao.profile.UserDAO;
import com.dicv.truck.response.Response;
import com.dicv.truck.util.DicvUtil;
import com.dicv.truck.util.PasswordEncryptor;
import com.dicv.truck.vo.UserLogin;

@Service
public class LoginService {

	@Autowired
	private UserDAO userProfileDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginService.class);

	public Response<UserLogin> doUserValidation(String userName, String password, ServletRequest request) {
		Response<UserLogin> response = new Response<UserLogin>();
		try {
			HttpServletRequest httpReq = (HttpServletRequest) request;
			UserProfile userProfile = userProfileDAO.doGetUserProfileDetails(userName);
			UserLogin userLogin = new UserLogin();
			if (userProfile == null) {
				response.setMessage(DicvConstants.USER_UNAVAILABLE);
				return response;

			}
			if (userProfile.getUserActive() == 1) {
				LOGGER.info("ACCOUNT for the USER {} is BLOCKED", userName);
				response.setMessage(DicvConstants.BLOCKED);
				userLogin.setToken(null);
				response.setData(userLogin);
			}
			if (PasswordEncryptor.encryptPassword(password).equals(userProfile.getPassword())) {
				userLogin.setUserId(userProfile.getUserId());
				userLogin.setUserRole(userProfile.getUserRole());
				userLogin.setUserName(userProfile.getUserName());
				response.setData(userLogin);
				response.setMessage(DicvConstants.SUCCESS);
			} else {
				response.setData(userLogin);
				userLogin.setToken(null);
				response.setMessage(DicvConstants.PASSWORD_INVALID);
			}
		} catch (Exception e) {
			LOGGER.info(DicvConstants.LOGIN_ERROR, e);
		}
		return response;

	}

}
