package com.dicv.truck.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dicv.truck.common.constants.DicvConstants;
import com.dicv.truck.common.exception.DicvTruckException;
import com.dicv.truck.dao.gpsImei.GpsDAO;
import com.dicv.truck.dao.pojo.DicvCategory;
import com.dicv.truck.dao.pojo.DicvType;
import com.dicv.truck.dao.pojo.DicvUser;
import com.dicv.truck.dao.pojo.GpsImei;
import com.dicv.truck.dao.pojo.UserProfile;
import com.dicv.truck.dao.pojo.Vehicle;
import com.dicv.truck.dao.profile.UserDAO;
import com.dicv.truck.dao.vehicle.VehicleDAO;
import com.dicv.truck.response.Pagination;
import com.dicv.truck.response.Response;
import com.dicv.truck.response.ResponseStatus;
import com.dicv.truck.util.DicvUtil;
import com.dicv.truck.util.RecordState;
import com.dicv.truck.vo.CategoryVO;
import com.dicv.truck.vo.TypeVO;
import com.dicv.truck.vo.VehicleVO;

@Service
public class VehicleService {

	@Autowired
	private VehicleDAO vehicleDAO;

	@Autowired
	private GpsDAO gpsDAO;

	@Autowired
	private UserDAO userProfileDAO;

	private static final Logger LOGGER = Logger.getLogger(VehicleService.class);

	public Response<VehicleVO> getVehicleDetails(Integer userId, Integer vehicleId) {
		Response<VehicleVO> response = new Response<VehicleVO>();
		try {
			Vehicle veh = vehicleDAO.getVehicleDetails(vehicleId);
			if (veh != null) {
				VehicleVO vehicleVO = new VehicleVO();
				vehicleVO.setRegistrationId(veh.getRegistrationId());
				vehicleVO.setGpsTranmission(false);
				if (veh.getGpsImei() != null) {
					vehicleVO.setGpsImei(veh.getGpsImei().getGpsImei());
					vehicleVO.setGpsSimNumber(veh.getGpsImei().getGpsSimNumber());
					if (veh.getVehicleUpdateTime() != null
							&& veh.getVehicleUpdateTime().after(DicvUtil.getPreviousDayTime(new Date()))) {
						vehicleVO.setGpsTranmission(true);
					} else {
						vehicleVO.setGpsTranmission(false);
					}
				}
				vehicleVO.setRegistrationId(veh.getRegistrationId());
				vehicleVO.setGpsTranmission(false);
				vehicleVO.setVehicleId(veh.getVehicleId());
				vehicleVO.setVin(veh.getVin());
				vehicleVO.setDescription(veh.getDescription());
				vehicleVO.setMaxPayLoadCapacity(veh.getMaxPayloadCapacity());
				vehicleVO.setMaxVehicleSpeed(veh.getVehicleMaxSpeed());
				if (veh.getDicvCategory() != null) {
					vehicleVO.setVehicleCategory(veh.getDicvCategory().getCategoryId());
					vehicleVO.setVehicleCategoryName(veh.getDicvCategory().getCategoryName());
				}
				if (veh.getDicvType() != null) {
					vehicleVO.setVehicleTypeId(veh.getDicvType().getTypeId());
					vehicleVO.setVehicleTypeName(veh.getDicvType().getTypeName());
				}
				vehicleVO.setVehicleCanParam(
						veh.getVehicleCanParam() != null && veh.getVehicleCanParam().getVehicleCanParamId() != null
								? true : false);

				if (veh != null) {
					response.setStatus(ResponseStatus.SUCCESS);
					response.setData(vehicleVO);
				}
			} else {
				response.setStatus(ResponseStatus.WARNING);
				response.setMessage(DicvConstants.NO_RECORDS_FOUND);
			}
		} catch (Exception ex) {
			LOGGER.info("Exception in Getting Vehicle  " + ex);
			ex.printStackTrace();
			response.setStatus(ResponseStatus.WARNING);
			response.setMessage(DicvConstants.NO_RECORDS_FOUND);
		}
		return response;
	}

	public Response<Integer> createVehicle(Integer userId, VehicleVO vehicleVO) {
		Response<Integer> response = new Response<Integer>();
		try {
			if (vehicleVO != null && userId != null && vehicleVO.getGpsImei() != null
					&& vehicleVO.getGpsSimNumber() != null && vehicleVO.getRegistrationId() != null
					&& vehicleVO.getVin() != null) {

				if (vehicleVO.getVin().length() != 17) {
					return new Response<>(ResponseStatus.WARNING, DicvConstants.VIN_LENGTH);
				}

				if (!vehicleDAO.checkRegistrationAvailable(vehicleVO.getRegistrationId(), vehicleVO.getVehicleId())) {
					return new Response<>(ResponseStatus.WARNING, DicvConstants.REG_NOT_AVAILABLE);
				}
				if (!vehicleDAO.checkVINAvailable(vehicleVO.getVin(), vehicleVO.getVehicleId())) {
					return new Response<>(ResponseStatus.WARNING, DicvConstants.VIN_NOT_AVAILABLE);
				}
				// Validation For GPS Tranmission
				if (gpsDAO.checkImeiExist(vehicleVO.getGpsImei(), vehicleVO.getVehicleId())) {
					return new Response<>(ResponseStatus.WARNING, DicvConstants.IMEI_ALREADY_EXIST);
				}

				UserProfile user = userProfileDAO.doGetUserProfileDetails(userId);
				if (user != null) {

					Vehicle vehicle = new Vehicle();
					GpsImei gpsImei = new GpsImei();
					if (vehicleVO.getVehicleId() != null) {
						LOGGER.info("Update Vehicle Details " + vehicleVO);
						vehicle = vehicleDAO.getVehicleDetails(vehicleVO.getVehicleId());
						gpsImei = vehicle.getGpsImei();
						gpsImei.setDicvUserUpdatedBy(2);
						gpsImei.setUpdatedDate(DicvUtil.getTimestamp());
						vehicle.setModifiedDateTime(DicvUtil.getTimestamp());
					} else {
						vehicle = new Vehicle();
						vehicle.setEnabled(RecordState.IS_NOT_ENABLE.getState());
						vehicle.setIsDeleted(RecordState.IS_NOT_DELETED.getState());
						vehicle.setVehicleStatus("AVAILABLE");
						vehicle.setRunningStatus("NEW");
						gpsImei.setVehicle(vehicle);
						gpsImei.setCreatedDate(DicvUtil.getTimestamp());
						gpsImei.setDicvUserCreatedBy(2);
						vehicle.setCreatedBy(2);
						vehicle.setUserId(2);
						vehicle.setModifiedDateTime(DicvUtil.getTimestamp());
						LOGGER.info("Add Vehicle Details " + vehicleVO);
					}
					vehicle.setDicvType(null);
					vehicle.setDicvCategory(null);
					if (vehicleVO.getVehicleTypeId() != null) {
						DicvType dicvType = vehicleDAO.getType(vehicleVO.getVehicleTypeId(), 2);
						if (null == dicvType) {
							return new Response<>(ResponseStatus.WARNING, DicvConstants.VALID_VEH_TYPE);
						}
						vehicle.setDicvType(dicvType);
					}

					if (vehicleVO.getVehicleCategory() != null) {
						DicvCategory category = vehicleDAO.getCategory(vehicleVO.getVehicleCategory(), 2);
						if (null == category) {
							return new Response<>(ResponseStatus.WARNING, DicvConstants.CATEGORY_VALID);
						}
						vehicle.setDicvCategory(category);
					}
					gpsImei.setGpsSimNumber(vehicleVO.getGpsSimNumber());
					gpsImei.setGpsImei(vehicleVO.getGpsImei());
					vehicle.setGpsImei(gpsImei);
					vehicle.setRegistrationId(vehicleVO.getRegistrationId());
					vehicle.setVin(vehicleVO.getVin());
					vehicle.setDescription(vehicleVO.getDescription());
					vehicle.setMaxPayloadCapacity(vehicleVO.getMaxPayLoadCapacity());
					if (vehicleVO.getPurchaseDate() != null) {
						vehicle.setPurchaseDate(
								new Timestamp(DicvUtil.stringToDate(vehicleVO.getPurchaseDate()).getTime()));
					}
					vehicle.setVehicleMaxSpeed(vehicleVO.getMaxVehicleSpeed());
					vehicle.setUserProfile(user);
					Integer vehicleId = vehicleDAO.crudVehicle(vehicle);
					response.setStatus(ResponseStatus.SUCCESS);
					response.setData(vehicleId);
					response.setMessage(vehicleVO.getVehicleId() != null ? DicvConstants.VEHICLE_UPDATE
							: DicvConstants.VEHICLE_ADD);
					return response;

				} else {
					response.setMessage(DicvConstants.USER_NO_PREVILAGE);
				}
			} else {
				response.setMessage(DicvConstants.MANDATORY_FIELDS_MISSING);
			}
			response.setStatus(ResponseStatus.WARNING);
		} catch (DicvTruckException ex) {
			LOGGER.info("Exception in Crud Vehicle  " + ex);
			response.setStatus(ResponseStatus.WARNING);
			response.setMessage(DicvConstants.NO_RECORDS_FOUND);
		} catch (Exception ex) {
			LOGGER.info("Exception in Crud Vehicle  " + ex);
			response.setStatus(ResponseStatus.WARNING);
			response.setMessage(DicvConstants.NO_RECORDS_FOUND);
		}
		return response;
	}

	public Response<List<VehicleVO>> getVehicleList(Integer userId) {
		Response<List<VehicleVO>> response = new Response<List<VehicleVO>>();
		try {
			List<VehicleVO> vehicleVOList = new ArrayList<VehicleVO>();
			LOGGER.info("Geting Vehicle List");
			List<Vehicle> vehicleList = vehicleDAO.getVehicleList(userId);
			if (vehicleList != null && vehicleList.size() > 0) {
				LOGGER.info("Vehicle List Total " + vehicleList.size());
				VehicleVO vehicleVO = new VehicleVO();
				for (Vehicle veh : vehicleList) {
					vehicleVO = new VehicleVO();
					vehicleVO.setRegistrationId(veh.getRegistrationId());
					vehicleVO.setGpsTranmission(false);
					if (veh.getGpsImei() != null) {
						vehicleVO.setGpsImei(veh.getGpsImei().getGpsImei());
						vehicleVO.setGpsSimNumber(veh.getGpsImei().getGpsSimNumber());
						if (veh.getVehicleUpdateTime() != null
								&& veh.getVehicleUpdateTime().after(DicvUtil.getPreviousDayTime(new Date()))) {
							vehicleVO.setGpsTranmission(true);
						} else {
							vehicleVO.setGpsTranmission(false);
						}
					}
					vehicleVO.setVehicleId(veh.getVehicleId());
					vehicleVO.setVin(veh.getVin());
					vehicleVO.setDescription(veh.getDescription());
					vehicleVO.setMaxPayLoadCapacity(veh.getMaxPayloadCapacity());
					vehicleVO.setMaxVehicleSpeed(veh.getVehicleMaxSpeed());
					if (veh.getDicvCategory() != null) {
						vehicleVO.setVehicleCategory(veh.getDicvCategory().getCategoryId());
						vehicleVO.setVehicleCategoryName(veh.getDicvCategory().getCategoryName());
					}
					if (veh.getDicvType() != null) {
						vehicleVO.setVehicleTypeId(veh.getDicvType().getTypeId());
						vehicleVO.setVehicleTypeName(veh.getDicvType().getTypeName());
					}
					vehicleVO.setVehicleCanParam(
							veh.getVehicleCanParam() != null && veh.getVehicleCanParam().getVehicleCanParamId() != null
									? true : false);
					vehicleVOList.add(vehicleVO);
				}
				LOGGER.info("Completed Vehicle List Process");
				if (vehicleVOList != null && vehicleVOList.size() > 0) {
					response.setStatus(ResponseStatus.SUCCESS);
					response.setData(vehicleVOList);
					response.setPageDetails(new Pagination(0, vehicleList.size(), vehicleList.size()));
				}
			} else {
				response.setStatus(ResponseStatus.WARNING);
				response.setMessage(DicvConstants.NO_RECORDS_FOUND);
			}

		} catch (Exception ex) {
			LOGGER.info("Exception in Getting Vehicle List " + ex.getMessage());
			response.setStatus(ResponseStatus.WARNING);
			response.setMessage(DicvConstants.NO_RECORDS_FOUND);
		}
		return response;

	}

	public Response<List<TypeVO>> getTypeList(Integer userId) {
		Response<List<TypeVO>> response = new Response<List<TypeVO>>();
		try {
			List<TypeVO> typeVOList = new ArrayList<TypeVO>();
			DicvUser user = userProfileDAO.getRootAdminUser();
			List<DicvType> typeList = vehicleDAO.getTypeList(user.getUserId());
			if (typeList != null && typeList.size() > 0) {
				TypeVO typeVO = new TypeVO();
				for (DicvType type : typeList) {
					typeVO = new TypeVO();
					typeVO.setTypeId(type.getTypeId());
					typeVO.setTypeName(type.getTypeName());
					typeVOList.add(typeVO);
				}
				response.setStatus(ResponseStatus.SUCCESS);
				response.setData(typeVOList);

			} else {
				response.setStatus(ResponseStatus.WARNING);
				response.setMessage(DicvConstants.NO_RECORDS_FOUND);
			}

		} catch (Exception ex) {
			LOGGER.info("Exception in Getting Type List " + ex.getMessage());
			ex.printStackTrace();
			response.setStatus(ResponseStatus.WARNING);
			response.setMessage(DicvConstants.NO_RECORDS_FOUND);
		}
		return response;

	}

	public Response<List<CategoryVO>> getCategoryList(Integer userId) {
		Response<List<CategoryVO>> response = new Response<List<CategoryVO>>();
		try {
			List<CategoryVO> categorVOList = new ArrayList<CategoryVO>();
			DicvUser user = userProfileDAO.getRootAdminUser();
			List<DicvCategory> categoryList = vehicleDAO.getCategoryList(user.getUserId());
			if (categoryList != null && categoryList.size() > 0) {
				CategoryVO categoryVO = new CategoryVO();
				for (DicvCategory dicvCategory : categoryList) {
					categoryVO = new CategoryVO();
					categoryVO.setCategoryId(dicvCategory.getCategoryId());
					categoryVO.setCategoryName(dicvCategory.getCategoryName());
					categorVOList.add(categoryVO);
				}
				response.setStatus(ResponseStatus.SUCCESS);
				response.setData(categorVOList);

			} else {
				response.setStatus(ResponseStatus.WARNING);
				response.setMessage(DicvConstants.NO_RECORDS_FOUND);
			}

		} catch (Exception ex) {
			LOGGER.info("Exception in Getting Type List " + ex.getMessage());
			ex.printStackTrace();
			response.setStatus(ResponseStatus.WARNING);
			response.setMessage(DicvConstants.NO_RECORDS_FOUND);
		}
		return response;

	}

}
