package com.dicv.truck.context;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationContextHolder implements ApplicationContextAware {

	private static ApplicationContext appContext;

	private static final Logger LOGGER = Logger.getLogger(ApplicationContextHolder.class);

	public void setApplicationContext(ApplicationContext arg0) {
		LOGGER.info("setting context file ........");
		appContext = arg0;
	}

	public ApplicationContext getAppContext() {

		return appContext;
	}

}
