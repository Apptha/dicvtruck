package com.dicv.truck.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_EMPTY)
public class Response<T> {

	ResponseStatus status;
	T data;
	Pagination pageDetails;
	String message;
	Warning warning;
	ErrorBean error;

	public ResponseStatus getStatus() {
		return status;
	}

	public T getData() {
		return data;
	}

	public Response(ResponseStatus status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public Response() {
		// TODO Auto-generated constructor stub
	}

	public Pagination getPageDetails() {
		return pageDetails;
	}

	public String getMessage() {
		return message;
	}

	public Warning getWarning() {
		return warning;
	}

	public ErrorBean getError() {
		return error;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public void setData(T data) {
		this.data = data;
	}

	public void setPageDetails(Pagination pageDetails) {
		this.pageDetails = pageDetails;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setWarning(Warning warning) {
		this.warning = warning;
	}

	public void setError(ErrorBean error) {
		this.error = error;
	}

	public void setFailure(ErrorBean error) {
		status = ResponseStatus.FAILURE;
		this.error = error;
	}

	public void setSuccess(T data) {
		status = ResponseStatus.SUCCESS;
		this.data = data;
	}

}
