package com.dicv.truck.response;

public class Warning {

	private HifiCampusStatusCode code;
	private String message;

	/**
	* 
	*/
	public Warning() {

	}

	/**
	 * @param code
	 * @param message
	 */
	public Warning(HifiCampusStatusCode code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public void setCode(HifiCampusStatusCode code) {
		this.code = code;
	}

	/**
	 * 
	 * @return
	 */
	public HifiCampusStatusCode getCode() {
		return code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static Warning getWarningMessage(HifiCampusStatusCode code, String msg) {
		return new Warning(code, msg);
	}

}
