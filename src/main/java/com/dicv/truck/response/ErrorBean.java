package com.dicv.truck.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ErrorBean {

	public static final String SECURITY_NOT_AUTHENTICATED = "security.notauthenticated";

	public static final String SECURITY_NOT_AUTHORIZED = "security.notauthorized";

	public static final String SYSTEM_ERROR = "system.error";

	private HifiCampusStatusCode code;
	private String message;

	@JsonIgnore
	private Object[] params;

	public ErrorBean(final HifiCampusStatusCode code, final String message) {
		this.code = code;
		this.message = message;
	}

	public ErrorBean(final HifiCampusStatusCode code) {
		this.code = code;
	}

	public ErrorBean(final HifiCampusStatusCode code, final Object[] params) {
		this.code = code;
		this.params = params;
	}

	/**
	 * @return the code
	 */
	public HifiCampusStatusCode getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(HifiCampusStatusCode code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the params
	 */
	public Object[] getParams() {
		return params;
	}

	/**
	 * @param params
	 *            the params to set
	 */
	public void setParams(Object[] params) {
		this.params = params;
	}

	/**
	 * 
	 * @param code
	 * @param msg
	 * @return ErrorBean
	 */
	public static ErrorBean getErrorMessage(HifiCampusStatusCode code, String msg) {
		return new ErrorBean(code, msg);
	}

}
