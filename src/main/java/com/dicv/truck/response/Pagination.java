package com.dicv.truck.response;

public class Pagination {
	private int currentPageNum;
	private int pageCount;
	private int totalRecords;

	public Pagination() {

	}

	public Pagination(int currentPageNum, int pageCount, int totalPages) {
		super();
		this.currentPageNum = currentPageNum;
		this.pageCount = pageCount;
		this.totalRecords = totalPages;
	}

	public int getCurrentPageNum() {
		return currentPageNum;
	}

	public void setCurrentPageNum(int currentPageNum) {
		this.currentPageNum = currentPageNum;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Pagination [currentPageNum=");
		builder.append(currentPageNum);
		builder.append(", pageCount=");
		builder.append(pageCount);
		builder.append(", totalPages=");
		builder.append(totalRecords);
		builder.append("]");
		return builder.toString();
	}

}
