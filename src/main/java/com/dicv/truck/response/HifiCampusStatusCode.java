package com.dicv.truck.response;

public enum HifiCampusStatusCode {

	LOCKED(4000, "Locked"),

	EXPIRED(4001, "Expired"),

	NEWACCOUNT(4002, "New Account"),

	NOT_FOUND(4004, "Not Found"),

	AUTHENTICATIONFAILED(5000, "Authentication Failed"),

	UNAUTHORIZED(5001, "Authorisation Failed"),

	UPDATE_FAILED(5002, "Update Failed"),

	NOT_ACCEPTABLE(5010, "Not Acceptable"),

	NO_RECORDS(5020, "No Records Found"),

	INSERT_FAILED(5023, "Creation failed"),

	INVALID_COMPONENT_TYPE(5022, "Invalid component type"),

	APPLICATION_DOWN(5021, "Application Down"),

	EXCEPTION_FAILED(5025, "Expectation Failed"), FILE_NOT_FOUND(5028, "No file found"),

	NO_JSON_INPUT(5027, "No JSON Input"),

	NO_ACTIVITY_CODE(5028, "No activity code"),

	WEBSERVICE_UNREACHABLE(5029, "Webservice is unreachable"),

	INVALID_URL_REQUEST(5031, "Invalid request URL "),

	INVALID_SORTBY_REQUEST(5032, "Invalid sort by column"),

	INVALID_REQUEST(5036, "Invalid request URL"),

	EXCEPTION(6000, " Generic Exception"),

	CREATE_FAILTED(5003, "CREATION FAILED"),

	DUPLICATE_NAME(5004, "DUPLICATE_NAME"),

	DELETE_FAILED(5037, "DELETE_FAILED"),

	RESTRICTED(5038, "RESTRICTED"), 
	
	NOT_REQUIRED(5039, "NOT REQUIRED"),

	INVALID_TOKEN(5041, "Token expired or Invalid");

	private final int value;
	private final String reasonString;

	/**
	 *
	 * @param value
	 * @param reasonString
	 */
	private HifiCampusStatusCode(int value, String reasonString) {
		this.value = value;
		this.reasonString = reasonString;
	}

	/**
	 * Return the integer value of this status code.
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Return the reason reason of this status code.
	 */
	public String getReasonString() {
		return reasonString;
	}

	/**
	 * Return the enum constant of this type with the specified numeric value.
	 *
	 * @param statusCode
	 *            the numeric value of the enum to be returned
	 * @return the enum constant with the specified numeric value
	 * @throws IllegalArgumentException
	 *             if this enum has no constant for the specified numeric value
	 */
	public static HifiCampusStatusCode valueOf(int statusCode) {
		for (HifiCampusStatusCode status : values()) {
			if (status.value == statusCode) {
				return status;
			}
		}
		throw new IllegalArgumentException("No matching constant for [" + statusCode + "]");
	}

}
